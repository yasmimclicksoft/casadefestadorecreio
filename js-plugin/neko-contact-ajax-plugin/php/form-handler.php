﻿<?php
require '../../../phpmailer/PHPMailerAutoload.php';

if ( !isset( $_SESSION ) ) session_start();
if ( !$_POST ) exit;
if ( !defined( "PHP_EOL" ) ) define( "PHP_EOL", "\r\n" );

	// ALEXANDRE, FAÇA OS TESTES NO ARQUIVO "form-handler_bkp_04_03_2015.php" E DEIXE ESTE FUNCIONAL PARA O CLIENTE!!!
	// PATH: public_html/js-plugin/neko-contact-ajax-plugin/php/

$to = "contato@amigosdopapaimudancas.com.br";
//$to = "amigosdopapai@hotmail.com.br,";

$subject = "Contato Amigos do Papai Mudanças";

//print_r($_POST);

foreach ($_POST as $key => $value) {
    if (ini_get('magic_quotes_gpc'))
        $_POST[$key] = stripslashes($_POST[$key]);
    $_POST[$key] = htmlspecialchars(strip_tags($_POST[$key]));
}

// Assign the input values to variables for easy reference
$name      = @$_POST["name"];
$email     = @$_POST["email"];
$phone     = @$_POST["phone"];
$message   = @$_POST["comment"];
$verify    = @$_POST["verify"];

// Test input values for errors
$errors = array();
 //php verif name
if(isset($_POST["name"])){

        if (!$name) {
            $errors[] = "Você deve digitar um nome.";
        } elseif(strlen($name) < 2)  {
            $errors[] = "O nome deve ter no mínimo 2 caracteres.";
        }

}
    //php verif email
/*if(isset($_POST["email"])){
    if (!$email) {
        $errors[] = "Você deve digitar um email.";
    } else if (!validEmail($email)) {
        $errors[] = "Você deve digitar um email válido.";
    }
}*/
    //php verif phone
/*if(isset($_POST["phone"])){
    if (!$phone) {
        $errors[] = "Você deve digitar um número de telefone.";
    }elseif ( !is_numeric( $phone ) ) {
        $errors[]= 'Seu número de telefone deve conter apenas dígitos.';
    }
}*/



//php verif comment
if(isset($_POST["comment"])){
    if (strlen($message) < 10) {
        if (!$message) {
            $errors[] = "Você deve digitar uma mensagem.";
        } else {
            $errors[] = "A mensagem deve ter no mínimo 10 caracteres.";
        }
    }
}

    //php verif captcha
if(isset($_POST["verify"])){
    if (!$verify) {
        $errors[] = "Você deve digitar o código de segurança.";
    } else if (md5($verify) != $_SESSION['nekoCheck']['verify']) {
        $errors[] = "O código de segurança estava incorreto, tente novamente. ";
    }
}

if ($errors) {
        // Output errors and die with a failure message
    $errortext = "";
    foreach ($errors as $error) {
        $errortext .= '<li>'. $error . "</li>";
    }

    echo '<div class="alert alert-error">Ocorrem os seguintes erros:<br><ul>'. $errortext .'</ul></div>';

}else{

     $mailBody  = "Nome: $name" . PHP_EOL . PHP_EOL;
     $mailBody .= "Email: $email." . PHP_EOL . PHP_EOL;
     $mailBody .= (isset($phone) && !empty($phone))?"Telefone: $phone." . PHP_EOL . PHP_EOL:'';
     $mailBody .= (!empty($company))?'Company: '. PHP_EOL.$company. PHP_EOL . PHP_EOL:'';
     $mailBody .= (!empty($quoteType))?'project Type: '. PHP_EOL.$quoteType. PHP_EOL . PHP_EOL:'';
     $mailBody .= "Mensagem :" . PHP_EOL;
     $mailBody .= $message . PHP_EOL . PHP_EOL;

     $mail = new PHPMailer;
     $mail->isSMTP();                                      // Set mailer to use SMTP
     $mail->Host = 'mail.canguruhost.com.br';
     $mail->SMTPAuth = true;                               // Enable SMTP authentication
     $mail->Username = 'contato@amigosdopapaimudancas.com.br';                 // SMTP username
     $mail->Password = 'amigos@papai';                     // SMTP password
     $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
     $mail->Port = 587;                                    // TCP port to connect to
     $mail->From = $email;
     $mail->FromName = $name;
     $mail->addReplyTo($email);
     $mail->addAddress($to);
     $mail->isHTML(false);                                  // Set email format to HTML
     $mail->Subject = $subject;
     $mail->Body = $mailBody;
     $mail->AltBody = $mailBody;
     echo '<script language="javascript">
              document.getElementById("submit2").disabled = "disabled";
              document.getElementById("contactfrm2").reset();
               </script>';
     if($mail->send() == true) {
        echo '<div class="alert alert-success" id="contact-msg">Sua mensagem foi enviada com sucesso.</div>';
        echo '<script language="javascript">
              var t;
              t = setTimeout(location_index, 3000);
              function location_index() {
                    window.location.href="../../../../index.html";
              }
              </script>';
     } else {
             echo '<div class="alert alert-error" id="contact-msg">Sua mensagem nao pode ser enviada.
                       <p><pre>'.$mail->ErrorInfo.'</pre></p></div>';
     }
}

// FUNCTIONS
function validEmail($email) {
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
        $isValid = false;
    } else {
        $domain = substr($email, $atIndex + 1);
        $local = substr($email, 0, $atIndex);
        $localLen = strlen($local);
        $domainLen = strlen($domain);
        if ($localLen < 1 || $localLen > 64) {
            // local part length exceeded
            $isValid = false;
        } else if ($domainLen < 1 || $domainLen > 255) {
            // domain part length exceeded
            $isValid = false;
        } else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
            // local part starts or ends with '.'
            $isValid = false;
        } else if (preg_match('/\\.\\./', $local)) {
            // local part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
            // character not valid in domain part
            $isValid = false;
        } else if (preg_match('/\\.\\./', $domain)) {
            // domain part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
            // character not valid in local part unless
            // local part is quoted
            if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
                $isValid = false;
            }
        }
        if ($isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
            // domain not found in DNS
            $isValid = false;
        }
    }
    return $isValid;
}

?>
